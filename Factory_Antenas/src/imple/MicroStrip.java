package imple;

import inter.Antena;

public class MicroStrip implements Antena {

	public int patron = 3;
	public int swr = 0;
	public int potencia = 16;
	
	@Override
	public String tipoAntena() {
		return "Antena MicroStrip, Patron: "+patron+". SWR: "+swr+". Potencia: "+potencia;
	}

}
