import inter.Antena;

public class FactoryMethodMain {

	public static void main(String[] args) {

		GraficadorAntenas graficador = new GraficadorAntenas();
		
		Antena antena = graficador.diseñaAntena("Yagi");
		System.out.println(antena.tipoAntena());
		
		Antena antena1 = graficador.diseñaAntena("LogPeriodica");
		System.out.println(antena1.tipoAntena());
		
		Antena antena2 = graficador.diseñaAntena("MicroStrip");
		System.out.println(antena2.tipoAntena());
	}

}
