import imple.LogPeriodica;
import imple.MicroStrip;
import imple.Yagi;
import inter.Antena;

public class GraficadorAntenas {
	
	public Antena diseñaAntena(String antena) {
		
		if(antena.equalsIgnoreCase("Yagi"))
			return new Yagi();
		else if(antena.equalsIgnoreCase("LogPeriodica"))
			return new LogPeriodica();
		else if(antena.equalsIgnoreCase("MicroStrip"))
			return new MicroStrip();
		
		return null;
	}
}
